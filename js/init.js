$(function () {

    var _htmlTemplate = "<div class='pad-group'><h3 class='title' id='${id}'>${title}<span style='color:#ee2233;font-style:italic;display:${isNew}'>new</span></h3><div><p class='address'><strong>请求地址</strong></p><pre class='code'>${address}</pre>" +
        "<p class='address'><strong>请求方式</strong></p><pre class='method-text'>${method}</pre><p class='address'><strong>请求参数</strong></p>${reqRecords}" +
        "<p class='address' style='display: ${isReqBody}'><strong>json对象属性</strong></p><p>${bodyRecords}</p>"+
        "<p class='address'><strong>响应参数</strong></p>${resRecords}<p class='address'><strong>状态码描述</strong></p>${errorRecords}</div></div>";
    var _tablePreTemplate = "<table class='inline'><thead><tr><th>参数</th><th>类型</th><th>是否必填</th><th>最大长度</th><th>描述</th><th>示例值</th></tr></thead><tbody>";
    var _errorTablePreTemplate = "<table class='inline'><thead><tr><th>状态码</th><th>描述</th></tr></thead><tbody>";
    var _tableEndTemplate = "</tbody></table>";
	var param = getQueryVariable("no");
    var apiContent = $("#apiContent");
    var _data;
    var data = $.ajax({
        url: param+".json",
        type: "get",
        dataType: "json",
        async: false,
        success: function (data) {
            _data = data;
        }
    });
    if (_data) {
        for (index = 0; index < _data.length; index++) {
            var _subHtml = _htmlTemplate;
            _subHtml = _subHtml.replace("${id}", "id" + index);
            _subHtml = _subHtml.replace("${isNew}", _data[index]["new"] ? 'inline' : 'none');
            _subHtml = _subHtml.replace("${isReqBody}", _data[index]["reqBody"] ? 'block' : 'none');
            _subHtml = _subHtml.replace("${isReqBody}", _data[index]["reqBody"] ? 'block' : 'none');
            $("#menuList").append("<li><a href='#id" + index + "'>" + _data[index]["title"] + "</a><span style='" +
                "color: #ee2233;font-style:italic;display:" + (_data[index]["new"] ? 'inline' : 'none') + "'>new</span></li>")
            var temp = _data[index];
            $.each(temp, function (key, value) {
                _subHtml = _subHtml.replace("${" + key + "}", value);
            })
            var reqParams = temp.reqParams;
            var tempTable = _tablePreTemplate;
            if (reqParams) {
                for (row = 0; row < reqParams.length; row++) {
                    var _tr = "<tr><td>" + reqParams[row][0] + "</td><td>" + reqParams[row][1] + "</td><td>" + reqParams[row][2] + "</td><td>" + reqParams[row][3] +
                        "</td><td>" + reqParams[row][4] + "</td><td>" + reqParams[row][5] + "</td></tr>";
                    tempTable += _tr;
                }
            }
            tempTable += _tableEndTemplate;
            _subHtml = _subHtml.replace("${reqRecords}", tempTable);
            var reqParams = temp.reqBody;
            var tempTable = "";
            if (reqParams) {
                tempTable = _tablePreTemplate;
                for (row = 0; row < reqParams.length; row++) {
                    var _tr = "<tr><td>" + reqParams[row][0] + "</td><td>" + reqParams[row][1] + "</td><td>" + reqParams[row][2] + "</td><td>" + reqParams[row][3] +
                        "</td><td>" + reqParams[row][4] + "</td><td>" + reqParams[row][5] + "</td></tr>";
                    tempTable += _tr;
                }
                tempTable += _tableEndTemplate;
            }
            _subHtml = _subHtml.replace("${bodyRecords}", tempTable);


            var resParams = temp.resParams;
            var tempTable = _tablePreTemplate;
            if (resParams) {
                for (row = 0; row < resParams.length; row++) {
                    var _tr = "<tr><td>" + resParams[row][0] + "</td><td>" + resParams[row][1] + "</td><td>" + resParams[row][2] + "</td><td>" + resParams[row][3] +
                        "</td><td>" + resParams[row][4] + "</td><td>" + resParams[row][5] + "</td></tr>";
                    tempTable += _tr;
                }
            }
            tempTable += _tableEndTemplate;
            _subHtml = _subHtml.replace("${resRecords}", tempTable);
            var errorCodes = temp.errorCodes;
            var tempTable = _errorTablePreTemplate;
            if (errorCodes) {
                for (row = 0; row < errorCodes.length; row++) {
                    var _tr = "<tr><td>" + errorCodes[row][0] + "</td><td>" + errorCodes[row][1] + "</td></tr>";
                    tempTable += _tr;
                }
            }
            tempTable += _tableEndTemplate;
            _subHtml = _subHtml.replace("${errorRecords}", tempTable);

            _errorTablePreTemplate
            apiContent.append(_subHtml)

        }
    }
});

function getQueryVariable(variable)
{
       var query = window.location.search.substring(1);
       var vars = query.split("&");
       for (var i=0;i<vars.length;i++) {
               var pair = vars[i].split("=");
               if(pair[0] == variable){return pair[1];}
       }
       return(false);
}
